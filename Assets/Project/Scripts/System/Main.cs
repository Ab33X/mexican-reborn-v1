﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour {

    public static Main main;
    public GameObject[] cubes;
    int score = 0;
    public float startLifePoints = 100, lifePoints, timer;
    public Text scoreText, objectiveText, finalText;

    [Header ("Unity Stuff")]
    public Image healthBar;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if(main == null){
            main = this;
        }else{
            main.scoreText = this.scoreText;
            main.objectiveText = this.objectiveText;
            main.healthBar = this.healthBar;
            main.finalText = this.finalText;
            Destroy(gameObject);
            return;
        }
    }

    void Start () {
        lifePoints = startLifePoints;
    }

    void Update () {
        cubes = GameObject.FindGameObjectsWithTag ("Cube");
        foreach (GameObject cube in cubes) {
            if (cube.GetComponent<Animator> ().GetFloat ("Die") == 3 || cube.GetComponent<Guard> ().getPoints () <= 0) {
                Destroy (cube, 0.5f);
            }
        }
        if (lifePoints < 0) {
            lifePoints = 0;
        }
        if (lifePoints > startLifePoints) {
            lifePoints = startLifePoints;
        }
        if (lifePoints == 0 && finalText == null) {

            GameObject.FindWithTag ("Player").GetComponent<Animator> ().SetBool ("Die", true);
            timer += Time.deltaTime;
            if (timer >= 2.5f) {
                GameObject.FindWithTag ("Scripter").GetComponent<UI>().GameOver();
            }
        }
        if(finalText != null){
            ShowScore();
        }
    }

    public void RaiseScore (int s) {
        score += s;
        scoreText.text = "Score: " + score;
    }

    public void GetDamage (int d) {
        if (lifePoints >= 0 && lifePoints <= startLifePoints) {
            lifePoints -= d;
        }
        healthBar.fillAmount = lifePoints / startLifePoints;
    }

    public void GetHeal (int h) {
        if (lifePoints >= 0 && lifePoints <= startLifePoints) {
            lifePoints += h;
        }
        healthBar.fillAmount = lifePoints / startLifePoints;
    }

    public void ObjectiveReached () {
        objectiveText.text += " (X)";
    }

    public void TryAgain(){
        lifePoints = startLifePoints;
        score = 0;
    }

    public void ShowScore(){
        finalText.text = "Your final score is " + score;
    }
}