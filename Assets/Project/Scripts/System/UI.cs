﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {
    public void PlayGame () {
        SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
    }

    public void QuitGame () {
        Application.Quit ();
    }

    public void TryAgain () {
        Main.main.TryAgain();
        SceneManager.LoadScene ("Lvl1");
    }

    public void GameOver () {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        //SceneManager.LoadScene ("GameOver");
    }
}