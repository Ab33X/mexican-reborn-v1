﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Looker : MonoBehaviour {
    private float reset = 5;
    private bool movingDown;
    public GameObject[] cubes;

    void Update () {
        if (movingDown == false)
            transform.position -= new Vector3 (0, 0, 0.1f);
        else
            transform.position += new Vector3 (0, 0, 0.1f);

        if (transform.position.z > 10)
            movingDown = false;
        else if (transform.position.z < -10)
            movingDown = true;
        reset -= Time.deltaTime;

        if (reset < 0) {
            GetComponent<SphereCollider> ().enabled = true;
            cubes = GameObject.FindGameObjectsWithTag ("Cube");
            foreach (GameObject cube in cubes) {
                cube.GetComponent<Guard> ().enabled = false;
            }
        }
    }

    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "Player") {
            GetComponent<SphereCollider> ().enabled = false;
            reset = 5;
            cubes = GameObject.FindGameObjectsWithTag ("Cube");
            foreach (GameObject cube in cubes) {
                cube.GetComponent<Guard> ().enabled = true;
            }
        }
    }
}