﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour {
    float timer, x, z, y;
    public GameObject enemyPrefab, player;

    void Start(){
        player = GameObject.FindWithTag ("Player");
    }

    void Update () {
        timer += Time.deltaTime;

        if (timer >= 8f) {
            timer = 0;            
            x = Random.Range (player.transform.position.x-10f, player.transform.position.x+10f);
            z = Random.Range (player.transform.position.z-10f, player.transform.position.z+10f);
            Vector3 position = new Vector3(x,player.transform.position.y,z);
            Quaternion rotation = new Quaternion();
            Instantiate (enemyPrefab, position, rotation);            
            FindObjectOfType<AudioManager>().Play("CubeSpawn");
        }

    }
}