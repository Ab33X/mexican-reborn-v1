﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Guard : MonoBehaviour {
    public GameObject player;
    public NavMeshAgent navMesh;
    private float startPoints = 15;
    public float points;
    Animator anim;
    float timer;

    [Header("Unity Stuff")]
    public Image healthBar;
    // Start is called before the first frame update
    void Start () {
        navMesh = GetComponent<NavMeshAgent> ();
        player = GameObject.FindWithTag("Player");
        anim = gameObject.GetComponent<Animator>();
        points = startPoints;
    }

    // Update is called once per frame
    void Update () {
        navMesh.destination = player.transform.position;
    }

    public void receiveDamage () {
        if(player.GetComponent<Animator>().GetBool("Attack1")){
            points--;
        }else if(player.GetComponent<Animator>().GetBool("Attack2")){
            points-=3;
        }
        FindObjectOfType<AudioManager>().Play("Hit");
        healthBar.fillAmount = points/startPoints;
    }

    public float getPoints () {
        return points;
    }

    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "Player" && !player.GetComponent<Animator>().GetBool("Defend")) {
            anim.SetBool("Attack", true);
            timer += Time.deltaTime;
            if (timer >= 0.2f) {
                Main.main.GetDamage(2);
                player.GetComponent<Animator>().SetBool("Damage", true);
                FindObjectOfType<AudioManager>().Play("CubeAttack");
            }
        }else if (collision.gameObject.tag == "Player" && player.GetComponent<Animator>().GetBool("Defend")){
            timer += Time.deltaTime;
            if (timer >= 0.2f) {
                FindObjectOfType<AudioManager>().Play("Block");
            }
        }else{
            anim.SetBool("Attack", false);
            player.GetComponent<Animator>().SetBool("Damage", false);
        }
    }
}