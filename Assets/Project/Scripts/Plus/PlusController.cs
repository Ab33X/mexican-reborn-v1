﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusController : MonoBehaviour {

    private void OnTriggerEnter (Collider collision) {
        if (collision.gameObject.tag == "Player") {
            Main.main.RaiseScore (25);
            FindObjectOfType<AudioManager> ().Play ("CubeKill");
            Destroy (gameObject);
        }
    }

}