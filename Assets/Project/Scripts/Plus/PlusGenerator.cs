﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusGenerator : MonoBehaviour
{
    float timer, x, y, z;
    public GameObject plusPrefab, player;

    void Start(){
        player = GameObject.FindWithTag ("Player");
    }

    void Update () {
        timer += Time.deltaTime;

        if (timer >= 20f) {
            timer = 0;
            x = Random.Range (player.transform.position.x-4f, player.transform.position.x+4f);
            z = Random.Range (player.transform.position.z-4f, player.transform.position.z+4f);
            Vector3 position = new Vector3 (x, player.transform.position.y+0.5f, z);
            Instantiate (plusPrefab, position, plusPrefab.transform.rotation);
            FindObjectOfType<AudioManager> ().Play ("CubeSpawn");
        }

    }
}
