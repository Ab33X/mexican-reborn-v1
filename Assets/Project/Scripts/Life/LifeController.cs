﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeController : MonoBehaviour {

    private void OnTriggerEnter (Collider collision) {
        if (collision.gameObject.tag == "Player") {
            Main.main.GetHeal (10);
            FindObjectOfType<AudioManager> ().Play ("HealTake");
            Destroy (gameObject);
        }
    }

}