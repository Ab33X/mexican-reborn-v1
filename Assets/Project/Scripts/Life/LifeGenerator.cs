﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeGenerator : MonoBehaviour {
    float timer, x, y, z;
    public GameObject lifePrefab, player;

    void Start () {
        player = GameObject.FindWithTag ("Player");
    }

    void Update () {
        timer += Time.deltaTime;

        if (timer >= 10f) {
            timer = 0;
            x = Random.Range (player.transform.position.x-2f, player.transform.position.x+2f);
            z = Random.Range (player.transform.position.z-2f, player.transform.position.z+2f);
            Vector3 position = new Vector3 (x, player.transform.position.y+0.5f, z);
            Instantiate (lifePrefab, position, lifePrefab.transform.rotation);
            FindObjectOfType<AudioManager> ().Play ("HealSpawn");
        }

    }
}