﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    Animator anim;
    private Rigidbody rb;
    public LayerMask groundLayers;
    public float jumpForce = 7;
    public BoxCollider col;
    bool canJump, move, sprint;
    float timer1, timer2, timer3, timer4;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    void Movement(){
        //Move right
        if (Input.GetKey(KeyCode.D)){            
            gameObject.transform.Translate(5f * Time.deltaTime,0,0);
            move = true;
            anim.SetBool("Moving", true);
        }
        //Move left
        if (Input.GetKey(KeyCode.A)){
            gameObject.transform.Translate(-5f * Time.deltaTime,0,0);
            move = true;
            anim.SetBool("Moving", true);
        }
        //Move forwards
        if (Input.GetKey(KeyCode.W)){        
            gameObject.transform.Translate(0,0,5f * Time.deltaTime);
            move = true;
            anim.SetBool("Moving", true);
        }
        //Move backwards
        if (Input.GetKey(KeyCode.S)){
            gameObject.transform.Translate(0,0,-5f * Time.deltaTime);
            move = true;
            anim.SetBool("Moving", true);         
        }
        //Sprint right
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftShift)){     
            gameObject.transform.Translate(7f * Time.deltaTime,0,0);
        }
        //Sprint left
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift)){     
            gameObject.transform.Translate(-7f * Time.deltaTime,0,0);
        }
        //Sprint backwards
        if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.LeftShift)){
            gameObject.transform.Translate(0,0,-7f * Time.deltaTime);
        }
        //Sprint forwards
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift)){        
            gameObject.transform.Translate(0,0,7f * Time.deltaTime);
        }
        //Detect no moving
        if(!Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D)){
            move = false;
            anim.SetBool("Moving", false);
        }
        //Jumping
        if(canJump && Input.GetKeyDown(KeyCode.Space)){    
            canJump = false;
            rb.AddForce(Vector3.up*jumpForce, ForceMode.Impulse);
        }
        //Detect sprint / no sprint        
        if(Input.GetKey(KeyCode.LeftShift)){
            sprint = true;
            anim.SetBool("Sprint", true);    
        }else{
            sprint = false;
            anim.SetBool("Sprint", false);     
        }
        //Detect attack1
        if(Input.GetKey(KeyCode.Mouse0)){
            anim.SetBool("Attack1", true);
            timer1 += Time.deltaTime;
            if (timer1 >= 0.7f) {
                timer1 = 0;
                FindObjectOfType<AudioManager>().Play("Attack 1");
            }
        }else{
            anim.SetBool("Attack1", false);
        }
        //Detect attack2
        if(Input.GetKey(KeyCode.Mouse2)){
            anim.SetBool("Attack2", true);
            timer2 += Time.deltaTime;
            if (timer2 >= 0.65f) {
                timer2 = 0;
                FindObjectOfType<AudioManager>().Play("Attack 2");
            }
        }else{
            anim.SetBool("Attack2", false);
        }
        //Detect defend
        if(Input.GetKey(KeyCode.Mouse1)){
            anim.SetBool("Defend", true);
        }else{
            anim.SetBool("Defend", false);
        }
        //Walk or sprint audio
        if(move && sprint && canJump){
            timer3 += Time.deltaTime;
            if (timer3 >= 0.3f) {
                timer3 = 0;
                FindObjectOfType<AudioManager>().Play("Step");
            }
        }else if(move && canJump){
            timer4 += Time.deltaTime;
            if (timer4 >= 0.4f) {
                timer4 = 0;
                FindObjectOfType<AudioManager>().Play("Step");
            }
        }
    }

/*
    private void OnCollisionEnter (Collision collision) {
        if (collision.transform.tag == "Ground") {
            canJump = true;
        }
    }
*/
}