﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacks : MonoBehaviour {

    public GameObject player;
    Animator anim;
    bool attack1, attack2;
    float x, y, z;

    // Start is called before the first frame update
    void OnCollisionEnter (Collision col) {

        if (col.gameObject.tag == "Cube" && (attack1 || attack2)) {
            col.gameObject.GetComponent<Guard>().receiveDamage();
            if (col.gameObject.GetComponent<Guard> ().getPoints () <= 0) {
                Animator cubeAnim = col.gameObject.GetComponent<Animator>();
                cubeAnim.SetFloat("Die", 1);
            }
        }

    }

    void Start () {
        player = GameObject.FindWithTag ("Player");
        anim = player.GetComponent<Animator> ();
    }

    void Update () {
        attack1 = anim.GetBool ("Attack1");
        attack2 = anim.GetBool ("Attack2");
    }
}