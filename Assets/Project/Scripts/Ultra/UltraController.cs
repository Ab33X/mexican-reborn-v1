﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UltraController : MonoBehaviour {

    private void OnTriggerEnter (Collider collision) {
        if (collision.gameObject.tag == "Player") {
            Main.main.RaiseScore (100);
            FindObjectOfType<AudioManager> ().Play("Win");
            Destroy (gameObject);
            Main.main.ObjectiveReached();            
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

}